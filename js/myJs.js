var app = angular.module("reiki",['ngRoute'])

.config(['$routeProvider', function($routeProvider){
  $routeProvider.
    when('/main',{
      templateUrl: 'main.html',
      controller:'MainCtrl'
    }).
    when('/votrePraticienne',{
      templateUrl: 'votrePraticienne.html',
      controller:'MainCtrl'
    }).
    when('/stages',{
      templateUrl: 'stages.html',
      controller:'MainCtrl'
    }).
    when('/reiki',{
      templateUrl:'reiki.html',
      controller :'ReikiCtrl'

    }).
    when('/contact',{
      templateUrl: 'contact.html',
      controller:'ContactCtrl'
    }).
    otherwise({redirectTo:'/main'})
}])

.controller('MainCtrl', ['$scope', '$http', function($scope, $http){
    $scope.images = [
    {
      "image" : "chat.jpg",
      "titre" : "Prendre RDV"
      
    },
    { 
      "image":"bdc.jpg",
      "titre":"bienvenue"
    },
    {
        "image": "bdc2.jpg",
        "titre" : "bonjour"
    }
   
    ];

    $scope.articles = [
    {
    "titre" : "Le Reiki",
    "img" : "placeholder.PNG",
    "body" : "Supercilia imperator id Rheni navium obrutis attonitus obrutis multitudine inpossibile callibus id Rauracum nive difficultatibus plurimis vi imperator vetabantur undique."
    },
     {
    "titre" : "Le Reiki et moi",
    "img" : "placeholder.PNG",
    "body" : "Supercilia imperator id Rheni navium obrutis attonitus obrutis multitudine inpossibile callibus id Rauracum nive difficultatibus plurimis vi imperator vetabantur undique."
    },
     {
    "titre" : "La méditation",
    "img" : "placeholder.PNG",
    "body" : "Supercilia imperator id Rheni navium obrutis attonitus obrutis multitudine inpossibile callibus id Rauracum nive difficultatibus plurimis vi imperator vetabantur undique."
    }
    ];



}])

.controller('ReikiCtrl', ['$scope', '$http', function($scope, $http){
    $scope.hide = false; //variable globale
    $scope.testHide =  function() { 
      if($scope.hide == false){
        $scope.hide = true;
      }
      else{
        $scope.hide = false;
      }
    };
}])

.controller('ServicesCtrl', ['$scope', '$http', function($scope, $http){
  $http.get('services.json').then(function(response){
    console.log(response);
    $scope.services = response.data;
  });
}])

.controller('ContactCtrl', ['$scope','$http', function($scope, $http){
   $http.get('locations.json').then(function(response){
    $scope.locations = response.data;
  })
}]);




$(document).ready(function() {

//a chaque fois que l'on scrolle
$(window).scroll(function (){
  $('.unArticle').each(function(i){
    

    var bottomObject = $(window).scrollTop();
    console.log(bottomObject);


    if (bottomObject > 600 ) {
      $('.unArticle').fadeIn(3000);
    }
    if (bottomObject < 500 ){
        $('.unArticle').hide();
      }
    });
  });
});



















